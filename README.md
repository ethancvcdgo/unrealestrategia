# Estrategia

Developed with Unreal Engine 5

Risketos Base:
- Capacitat d’elecció, per exemple varies torretes.
- El HUD es crea per BP.
- Us d’esdeveniments.
- Enemics o objectius a on atacar i al fer-ho que morin.
- Enemics que segueixen un camí.
- Mort del jugador i restart

Risketos Extra:
- Control de oleadas, pudiendo elegir duración
- Aumento Progresivo de dificultad
- Economy in shambles
- Multinivel
- Menú inicial donde elegir nivel
